//app.js

const config = require("config");

App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    login();
  },

  data: {
    openid: '',
    user_token: '',
    user_email: '',
    avatar: '',
    wallet: '',
    invite_code: '',
    nickName: '',
    uname: '',
  }
})

function login() {
  console.log('logining..........');
  //调用登录接口
  wx.login({
    success: function (e) {
      console.log('wxlogin successd...now get code .....');
      var code = e.code;

      console.log(code);
      wx.getUserInfo({
        success: function (res) {
          console.log('app.js  wxgetUserInfo successd........');
          var encryptedData = encodeURIComponent(res.encryptedData);
          var signature = encodeURIComponent(res.signature);
          var iv = encodeURIComponent(res.iv);
          var rawData = encodeURIComponent(res.rawData);

          console.log('encryptedData = ' + encryptedData);
          console.log('signature = ' + signature);
          console.log('iv = ' + iv);
          console.log('rawData = ' + rawData);
          postSecretParams(signature, encryptedData, iv, rawData, code);//发送加密参数
        }
      })
    }
  });
}




/**
 * 发送加密参数
 */

function postSecretParams(signature, encryptedData, iv, rawData, code) {

  wx.request({
    url: config.basepath + 'users/wxapp_info.json',
    data: {
      signature: signature,
      encryptedData: encryptedData,
      iv: iv,
      code: code,
      rawData: rawData
    },
    method: 'POST',
    header: {
      'Content-Type': 'application/json'
    },
    dataType: 'json',

    success: function (res) {
      // TODO
      console.log('app.js postSecretParams success   ' + res.data);
      var json = res.data;
      console.log('unionId = ' + json.unionId);
      console.log('openid = ' + json.openid);
      console.log('nickName = ' + json.nickName);
      console.log('status = ' + json.status);

      console.log('json.user.email = ' + json.user.email);
     console.log('json.user.authentication_token = ' + json.user.authentication_token);
      console.log('json.user.avatar = ' + json.user.avatar);
      console.log('json.user.invite_code = ' + json.user.invite_code);
      console.log('json.user.uname = ' + json.user.uname);
      console.log('json.user._id.$oid = ' + json.user._id.$oid);

      wx.setStorageSync(config.openid, json.openid)
      wx.setStorageSync(config.unionId, json.unionId)
      wx.setStorageSync(config.nickName, json.nickName)
      if(json.status=="OK"){
          console.log('setStorageSync status')
          wx.setStorageSync(config.status, json.status)
      }
        
      

      wx.setStorageSync(config.user_email, json.user.email)
      wx.setStorageSync(config.user_token, json.user.authentication_token)
      wx.setStorageSync(config.avatar, json.user.avatar)
      wx.setStorageSync(config.wallet, json.user.wallet)
      wx.setStorageSync(config.invite_code, json.user.invite_code)
      wx.setStorageSync(config.uname, json.user.uname)
      wx.setStorageSync(config.userid, json.user._id.$oid)
      wx.setStorageSync(config.phone, json.user.phone)


    },
    fail: function () {
      // fail
      console.log('app.js postSecretParams fail');
    },
    complete: function () {
      // complete
      console.log('app.js postSecretParams complete');
    }
  })
}




