const config = require("../../config");
var user_email;
var user_token;
var self;
Page({
    onLoad: function (options) {
        self = this;
        try {
            var wallet = wx.getStorageSync(config.wallet)
            user_email = wx.getStorageSync(config.user_email)
            user_token = wx.getStorageSync(config.user_token)
        } catch (e) {
            // Do something when catch error
        }
        self.setData({
            'balance': wallet
        })
    },

    onShow: function () {
        try {
            var userid = wx.getStorageSync(config.userid)
        } catch (e) {
            // Do something when catch error
        }
        wx.request({
            url: config.basepath + 'users/'+userid+'.json',
            data: {},
            method: 'GET',
            // dataType:'jsonp',
            header: {
                "X-User-Email": user_email,
                "X-User-Token": user_token,
                'Content-Type': 'application/json'
            },
            success: function (res) {
                console.log("balance.js  request userinfo success" + res.data);
                var json = res.data;
                console.log("balance.js  request userinfo success wallet" + json.wallet);
                wx.setStorageSync(config.wallet,json.wallet)
                self.setData({
                    'balance': json.wallet
                })
            },
            fail: function () {
                // fail
                console.log("balance.js  request userinfo fail");
            },
            complete: function () {
                // complete
                console.log("balance.js  request userinfo complete");
            }
        })
    },
    data: {
        balance: ''
    },

    red: function () {
        console.log("balance.js  tored")
        wx.redirectTo({
            url: '../red/red',
        })
    },
    upper: function (e) {
        console.log(e)
    },
    lower: function (e) {
        console.log(e)
    },
    scroll: function (e) {
        console.log(e)
    },
    imageError: function (e) {
        console.log('image3发生error事件，携带值为', e.detail.errMsg)
    },


});

