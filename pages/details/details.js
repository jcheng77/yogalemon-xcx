const config = require("../../config");
var watch = "观看人数:";
var course_introd = '课程简介';
var that;
var uid;
var url;
var detailsurl;
var user_email;
var user_token;
var title;
var free;


Page({

    onShareAppMessage: function () {
        console.log("that.data.details.name" + that.data.details.name);
        console.log("that.data.details.price" + that.data.details.price);
        if (that.data.details.price == null) {
            return {
                title: that.data.details.name,
                desc: that.data.details.desc,
                path: '/pages/details/details?uid=' + url + '&free=free',
            }
        } else {
            return {
                title: that.data.details.name,
                desc: that.data.details.desc,
                path: '/pages/details/details?uid=' + url,
            }
        }

    },
    onShow: function () {
        console.log('details onShow');
        try {
            user_email = wx.getStorageSync(config.user_email)
            user_token = wx.getStorageSync(config.user_token)
        } catch (e) {

        }
        that.setData({
            email: user_email,
            url: url,
        });
        requestByWay(that);

    },
    data: {
        url: url,
        details: {},
        email: '',
    },

    onLoad: function (options) {
        console.log('details onLoad');
        this.videoContext = wx.createVideoContext('myVideo')
        that = this;
        title = options.title;
        url = config.basepath + 'channels/' + title + '.json';
        uid = options.uid;
        free = options.free;
        console.log('uid = ' + options.uid);
    },

    upper: function (e) {
        console.log(e)
    },
    lower: function (e) {
        console.log(e)
    },
    scroll: function (e) {
        console.log(e)
    },

    follow: function () {
        console.log("follow channel by id")
        follow(this, title);
    },
    history: function () {
        console.log("正在生成回放");
        wx.showToast({
            title: '正在生成回放',
            icon: 'success',
            duration: 500
        })
    },
    unlogin: function () {
        console.log("unlogin")
        wx.redirectTo({
            url: '../login/login?request=details',
        })
    },
    cat: function (e) {
        var id = e.currentTarget.id;
        console.log('id' + id);
        title = id;
        url = config.basepath + 'channels/' + title + '.json';
        requestByWay(that);
    }
});



/**
 * 从服务端获取数据
 */
function requsetData(url, _self) {


    wx.showToast({
        title: '正在加载',
        icon: 'loading',
        duration: 10000
    })
    if (user_email) {
        console.log('requsetData  with user email' + user_email)
        wx.request({
            url: url,
            method: 'GET',
            header: {
                "X-User-Email": user_email,
                "X-User-Token": user_token,
                'Content-Type': 'application/json'
            },
            success: function (res) {
                console.log("details request api success");
                console.log(res.data);
                _self.setData({
                    "details": res.data,
                });
                wx.setNavigationBarTitle({
                    title: res.data.name,
                })
            },
            fail: function () {
                console.log("details request api fail");
            },
            complete: function () {
                wx.hideToast();
                console.log("details request api complete");
            }
        })
    } else {
        console.log('requsetData  without user email' + user_email)
        console.log('requsetData  without user email' + url)

        wx.request({
            url: url,
            method: 'GET',
            header: {
                'Content-Type': 'application/json'
            },
            dataType: 'json',
            success: function (res) {
                console.log("details request api success");
                console.log(res.data);
            
                _self.setData({
                    "details": res.data,
                });
                wx.setNavigationBarTitle({
                    title: res.data.name,
                })
            },
            fail: function () {
                console.log("details request api fail");
            },
            complete: function () {
                wx.hideToast();
                console.log("details request api complete");
            }
        })
    }

}






function login() {
    console.log('logining..........');
    //调用登录接口
    wx.login({
        success: function (e) {
            console.log('wxlogin successd...now get code .....');
            var code = e.code;
            console.log(code);
            wx.getUserInfo({
                success: function (res) {
                    console.log('app.js  wxgetUserInfo successd........');
                    var encryptedData = encodeURIComponent(res.encryptedData);
                    var signature = encodeURIComponent(res.signature);
                    var iv = encodeURIComponent(res.iv);
                    var rawData = encodeURIComponent(res.rawData);

                    console.log('encryptedData = ' + encryptedData);
                    console.log('signature = ' + signature);
                    console.log('iv = ' + iv);
                    console.log('rawData = ' + rawData);
                    postSecretParams(signature, encryptedData, iv, rawData, code);//发送加密参数
                }
            })
        }
    });
}





/**
 * 发送加密参数
 */

function postSecretParams(signature, encryptedData, iv, rawData, code) {

    wx.request({
        url: config.basepath + 'users/wxapp_info.json',
        data: {
            signature: signature,
            encryptedData: encryptedData,
            iv: iv,
            code: code,
            rawData: rawData
        },
        method: 'POST',

        success: function (res) {
            // TODO
            console.log('app.js postSecretParams success' + res.data);

        },
        fail: function () {
            // fail
            console.log('app.js postSecretParams fail');
        },
        complete: function () {
            // complete
            console.log('app.js postSecretParams complete');
        }
    })
}




function follow(that, id) {
    wx.request({
        url: config.basepath + 'channels/' + id + '/follow.json',
        header: {
            "X-User-Email": user_email,
            "X-User-Token": user_token,
            'Content-Type': 'application/json'
        },
        dataType: 'json',  //TODO     返回状态
        method: 'POST',
        success: function (res) {
            console.log('details.js follow success' + res.data);
            wx.showToast({
                title: '预约成功',
                icon: 'success',
                duration: 500
            })
            requestByWay(that);
        },
        fail: function () {
            // fail
            console.log('details.js follow fail');
        },
        complete: function () {
            // complete
            console.log('details.js follow complete');
        }
    })

}





function like(that, id) {
    wx.request({
        url: config.basepath + 'channels/' + id + '/like.json',
        header: {
            "X-User-Email": user_email,
            "X-User-Token": user_token,
            'Content-Type': 'application/json'
        },
        dataType: 'jsonp',
        method: 'POST',
        success: function (res) {
            console.log('details.js like success' + res.data);
            wx.showToast({
                title: '收藏成功',
                icon: 'success',
                duration: 500
            })
            requestByWay(that);

        },

        fail: function () {
            // fail
            console.log('details.js like fail');
        },
        complete: function () {
            // complete
            console.log('details.js like complete');
        }
    })

}

/**
 * 封装各种进入方式的请求
 */
function requestByWay(that) {
    if (title != null) {//正常进入 显示课程 支付回显 登录回显
        console.log('url onShow titleurl' + url);
        requsetData(url, that);
    } else if (uid != null) {//分享进入
        console.log('details onShow 分享进入');
        if (free != null) {//免费课程
            console.log('details onShow 分享进入 free');
            requsetData(uid, that);
        } else {//收费课程
            console.log('details onShow 分享进入 not free');
            if (user_email) {//已经登录过
                console.log('已经登录过 user_email = ' + user_email);
                requsetData(uid, that);
            } else {//未登录过
                console.log('未登录过 user_email = ' + user_email);
                // login();
                wx.redirectTo({
                    url: '../login/login?request=details&detailsurl=' + uid,
                })
            }
        }
    }
}