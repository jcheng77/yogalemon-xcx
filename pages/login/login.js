const config = require("../../config");

var request;
var request_wallet = 'wallet';
var request_details = 'details';
var request_purchased = 'purchased';
var request_red = 'red';
var request_login = 'login';
var detailsurl;
var user_email;
var user_token;
var phone;
var status;

Page({

  onLoad: function (options) {
    request = options.request;
    detailsurl = options.detailsurl;
  },


  onShow: function () {
    try {
      user_email = wx.getStorageSync(config.user_email)
      user_token = wx.getStorageSync(config.user_token)
      phone = wx.getStorageSync(config.phone)
      status = wx.getStorageSync(config.status)


    } catch (e) {

    }
  },
  data: {
    phone: '',
    verfyCode: '',
    disabled: false,
    second: 60
  },



  //用户名和密码输入框事件
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  verfyCodeInput: function (e) {
    this.setData({
      verfyCode: e.detail.value
    })
  },



  getcode: function () {
    if (this.data.phone.length == 0) {
      wx.showToast({
        title: '请输入手机号',
        icon: 'warn',
        duration: 500
      })
      return;
    }
    var self = this;
    var reg = /^1[0-9]{10}$/;
    var isPhone = reg.test(self.data.phone);
    if (!isPhone) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'warn',
        duration: 500
      })
      return;
    }
    console.log(self.data.phone);
    if (status) {
      sendCodeWithHeader(self);
    } else {
      sendCodeWithoutHeader(self);
    }

  },


  buttonlogin: function () {

    if (this.data.phone.length == 0) {
      wx.showToast({
        title: '请输入手机号',
        icon: 'warn',
        duration: 500
      })
      return;
    }
    if (this.data.verfyCode.length == 0) {
      wx.showToast({
        title: '请输入验证码',
        icon: 'warn',
        duration: 500
      })
      return;
    }

    var self = this;

    console.log(self.data.phone);
    console.log(self.data.verfyCode);


    if (status) {
      loginWithHeader(self);
    } else {
      loginWithoutHeader(self);
    }

  }








});


/**
 * 倒计时
 */
function countdown(that) {
  var second = that.data.second
  if (second == 0) {
    that.setData({
      disabled: false,
      second: 60,
    });
    return;
  }
  var time = setTimeout(function () {
    that.setData({
      second: second - 1
    });
    countdown(that);
  }
    , 1000)
}



/**
 * 登录后处理
 */
function dologin() {

  if (request != null) {
    console.log('request = ' + request)
    if (request == request_wallet) {
      wx.redirectTo({
        url: '../balance/balance',
      })
    } else if (request == request_details) {
      wx.navigateBack({
        delta: 1,
      })
    } else if (request == request_red) {
      wx.redirectTo({
        url: '../red/red',
      })
    } else if (request == request_purchased) {
      wx.redirectTo({
        url: '../purchased/purchased',
      })
    } else if (request == request_login) {
      wx.navigateBack({
        delta: 1
      });
    }
  }
}



function loginWithHeader(self) {
  wx.request({
    url: config.basepath + 'users/verify_phone.json',
    data: {
      phone: self.data.phone,
      code: self.data.verfyCode,
    },
    method: 'GET',
    dataType: 'json',
    header: {
      'Content-Type': 'application/json',
      "X-User-Email": user_email,
      "X-User-Token": user_token,
    }, // 设置请求的 header
    success: function (res) {
      // success
      console.log(' login login success' + res.data)
      var json = res.data;
      if (json.status == "OK") {
        wx.showToast({
          title: '登录成功',
          icon: 'success',
          duration: 500
        })
        wx.setStorageSync(config.status, json.status)
        wx.setStorageSync(config.user_email, json.user.email)
        wx.setStorageSync(config.user_token, json.user.authentication_token)
        wx.setStorageSync(config.avatar, json.user.avatar)
        wx.setStorageSync(config.wallet, json.user.wallet)
        wx.setStorageSync(config.invite_code, json.user.invite_code)
        wx.setStorageSync(config.uname, json.user.uname)
        wx.setStorageSync(config.userid, json.user._id.$oid)
        wx.setStorageSync(config.phone, json.user.phone)


        setTimeout(function () {
          dologin();
        }, 1000)


      } else if (json.status == "FAIL") {
        wx.showToast({
          title: '登录失败',
          icon: 'success',
          duration: 500
        })
      }

    },
    fail: function () {
      // fail
      console.log('login login fail')
    },
    complete: function () {
      // complete
      console.log('login login complete')
    }
  })
}




function loginWithoutHeader(self) {
  wx.request({
    url: config.basepath + 'users/verify_phone.json',
    data: {
      phone: self.data.phone,
      code: self.data.verfyCode,
    },
    method: 'GET',
    dataType: 'json',
    header: {
      'Content-Type': 'application/json'
    }, // 设置请求的 header
    success: function (res) {
      // success
      console.log(' login login success' + res.data)
      var json = res.data;
      if (json.status == "OK") {
        wx.showToast({
          title: '登录成功',
          icon: 'success',
          duration: 500
        })
        wx.setStorageSync(config.status, json.status)
        wx.setStorageSync(config.user_email, json.user.email)
        wx.setStorageSync(config.user_token, json.user.authentication_token)
        wx.setStorageSync(config.avatar, json.user.avatar)
        wx.setStorageSync(config.wallet, json.user.wallet)
        wx.setStorageSync(config.invite_code, json.user.invite_code)
        wx.setStorageSync(config.uname, json.user.uname)
        wx.setStorageSync(config.userid, json.user._id.$oid)
        wx.setStorageSync(config.phone, json.user.phone)

        setTimeout(function () {
          dologin();
        }, 1000)


      } else if (json.status == "FAIL") {
        wx.showToast({
          title: '登录失败',
          icon: 'success',
          duration: 500
        })
      }

    },
    fail: function () {
      // fail
      console.log('login login fail')
    },
    complete: function () {
      // complete
      console.log('login login complete')
    }
  })
}



function sendCodeWithoutHeader(self) {
  wx.request({
    url: config.basepath + 'users/send_code.json',
    data: { phone: self.data.phone },
    method: 'GET',
    dataType: 'json',
    // header: {}, // 设置请求的 header
    success: function (res) {
      // success
      console.log(' login get_code success' + res.data)
      wx.showToast({
        title: '验证码已发送',
        icon: 'success',
        duration: 500
      })

      self.setData({
        disabled: true
      });
      countdown(self);

    },
    fail: function () {
      // fail
      console.log('login get_code fail')
    },
    complete: function () {
      // complete
      console.log('login get_code complete')
    }
  })
}



function sendCodeWithHeader(self) {
  wx.request({
    url: config.basepath + 'users/send_code.json',
    data: { phone: self.data.phone },
    method: 'GET',
    dataType: 'json',
    header: {
      'Content-Type': 'application/json',
      "X-User-Email": user_email,
      "X-User-Token": user_token,
    },
    success: function (res) {
      // success
      console.log(' login get_code success' + res.data)
      wx.showToast({
        title: '验证码已发送',
        icon: 'success',
        duration: 500
      })

      self.setData({
        disabled: true
      });
      countdown(self);

    },
    fail: function () {
      // fail
      console.log('login get_code fail')
    },
    complete: function () {
      // complete
      console.log('login get_code complete')
    }
  })
}