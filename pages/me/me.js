const config = require("../../config");

var user_email;
var avatar;
var uname;
var status;
var phone;



Page({

    data: {
        avatar: '',
        email: '',
        uname: '',
        open: true,
        status: '',
        phone:'',
    },


    onShow: function () {
        console.log("onShow center")
        try {
            user_email = wx.getStorageSync(config.user_email)
            avatar = wx.getStorageSync(config.avatar)
            uname = wx.getStorageSync(config.uname)
            status = wx.getStorageSync(config.status)
            phone = wx.getStorageSync(config.phone)


        } catch (e) {
            // Do something when catch error
            console.log(e)
        }
        var self = this;
        self.setData({
            'avatar': avatar,
            'email': user_email,
            'uname': uname,
            'status': status,
            'phone':phone,
        });

        wx.request({
            url: config.basepath + 'users/wx_lite.json',
            data: {},
            method: 'GET',
            success: function (res) {
                console.log("index.js  request index api success" + res.data);
                self.setData({
                    "open": res.data,
                });
            },

        })


    },

    login: function () {
        console.log('user.email = ' + user_email)
        console.log('user.status = ' + status)
        if (status) {
            if (phone) {
                wx.showToast({
                    title: '已经登录',
                    icon: 'success',
                    duration: 500
                })
            } else {
                wx.navigateTo({
                    url: '../login/login?request=login'
                })
            }
        } else {
            if (user_email) {
                wx.showToast({
                    title: '已经登录',
                    icon: 'success',
                    duration: 500
                })
            } else {
                wx.navigateTo({
                    url: '../login/login?request=login'
                })
            }
        }



    },
    wallet: function () {
        if (user_email) {
            wx.navigateTo({
                url: '../balance/balance'
            })
        } else {
            wx.navigateTo({
                url: '../login/login?request=wallet'
            })
        }
    },

    purchased: function () {
        if (user_email) {
            wx.navigateTo({
                url: '../purchased/purchased'
            })
        } else {
            wx.navigateTo({
                url: '../login/login?request=purchased'
            })
        }
    },
    red: function () {
        if (user_email) {
            wx.navigateTo({
                url: '../red/red'
            })
        } else {
            wx.navigateTo({
                url: '../login/login?request=red'
            })
        }
    },
    upper: function (e) {
        console.log(e)
    },
    lower: function (e) {
        console.log(e)
    },
    scroll: function (e) {
        console.log(e)
    },
    imageError: function (e) {
        console.log('image3发生error事件，携带值为', e.detail.errMsg)
    },



});

