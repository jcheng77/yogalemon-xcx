require('../../utils/util.js');
var Pingpp = require('../../utils/pingpp.js')
const config = require("../../config");
var title = "课程";
var pay = '去支付';
var discount = '优惠';
var unused = '未使用 >';
var coupon_number = '个优惠券可用';
var text_balance = '帐号余额';
var text_total_price = '课程总价';
var text_need_pay = '还需支付';


var url;
var price;
var need_pay;
var discount;
var openid;
var user_email;
var user_token;



Page({

    onLoad: function (options) {
        url = config.basepath + 'channels/' + options.id + '/checkout.json';
        console.log(url);
        price = options.price;
        try {
            var wallet = wx.getStorageSync(config.wallet)
            user_email = wx.getStorageSync(config.user_email)
            user_token = wx.getStorageSync(config.user_token)
            openid = wx.getStorageSync(config.openid)
        } catch (e) {
            // Do something when catch error
        }
        var _self = this;

        if (price > 10) {
            need_pay = price - wallet;
            if(need_pay<0){
                need_pay=0;
            }
            console.log("need pay "+need_pay)
            console.log("price "+price)
            console.log("wallet "+wallet)

        } else {
            need_pay = price;
             console.log("need pay "+need_pay)
            console.log("price "+price)
                    console.log("wallet "+wallet)
            _self.setData({
                'forbid': true,
            });
            console.log("forbid "+true)
        }
        _self.setData({
            "price": options.price,
            "host_name": options.host_name,
            "name": options.name,
            "host_avatar": options.host_avatar,
            'wallet': wallet,
            'need_pay': need_pay,
        });


    },



    data: {
        title: title,
        price: '',
        host_name: '',
        name: '',
        host_avatar: '',
        id: '',
        pay: pay,
        wallet: '',
        discount: discount,
        coupon_number: coupon_number,
        unused: unused,
        text_balance: text_balance,
        text_total_price: text_total_price,
        text_need_pay: text_need_pay,
        need_pay: '',
        forbid: '',
    },

    toast: function () {
        if (openid) {
            payFunction();
        } else {
            wx.login({
                success: function (e) {
                    console.log('wxlogin successd...now get code .....');
                    var code = e.code;
                    console.log('code'+code);
                     postSecretParams(code);
                }
            });
        }
    },

});



function payFunction() {

    wx.request({
        url: url,
        data: {
            pay_channel: 'wx_lite',//渠道名
            amount: need_pay,//支付金额
            open_id: openid, //之前获取到的open_id
        },
        method: 'POST',
        dataType: 'json',
        header: {
            "X-User-Email": user_email,
            "X-User-Token": user_token,
            'Content-Type': 'application/json'
        },

        success: function (res) {
            console.log('checkout result' + res.data);
            if (res.data == "success") {
                wx.showToast({
                    title: '使用余额支付成功',
                    icon: 'success',
                    duration: 500
                })
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1,
                    })
                }, 1000)
                return;
            }
            var charge = res.data;
            var json = charge;
            console.log('checkout status' + json.status);
            if (json.status != undefined && json.status == "success") {
                wx.showToast({
                    title: '您已购买该课程',
                    icon: 'success',
                    duration: 500
                });
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1,
                    })
                }, 1000)

                return;
            } else {
                Pingpp.createPayment(charge, function (result, err) {
                    console.log('Pingpp result' + result);
                    console.log('Pingpp err msg' + err.msg);
                    console.log('Pingpp err extra' + err.extra);
                    if (result == "success") {
                        wx.showToast({
                            title: '支付成功',
                            icon: 'success',
                            duration: 500
                        })
                        setTimeout(function () {
                            wx.navigateBack({
                                delta: 1,
                            })
                        }, 1000)
                        // 只有微信小程序 wx_lite 支付成功的结果会在这里返回
                    } else if (result == "fail") {
                        // charge 不正确或者微信小程序支付失败时会在此处返回
                    } else if (result == "cancel") {
                        // 微信小程序支付取消支付
                    }
                });

            }

        }
    })
}












/**
 * 发送加密参数
 */

function postSecretParams(code) {

    wx.request({
        url: config.basepath + 'users/wxapp_info.json',
        data: {
            code: code,
        },
        method: 'POST',
        header: {
            'Content-Type': 'application/json'
        },
        dataType: 'json',

        success: function (res) {
            // TODO
            console.log('app.js postSecretParams success   ' + res.data);
            var json = res.data;
            wx.setStorageSync(config.openid, json.openid)
            console.log('openid = '+json.openid)
            openid = json.openid;
            payFunction();

        },
        fail: function () {
            // fail
            console.log('app.js postSecretParams fail');
        },
        complete: function () {
            // complete
            console.log('app.js postSecretParams complete');
        }
    })
}