var status = ' 已结束';
var watch = "观看人数:";
const config = require("../../config");


Page({


    onLoad: function (options) {
         try {
            var user_email = wx.getStorageSync(config.user_email)
            var user_token = wx.getStorageSync(config.user_token)

        } catch (e) {
            // Do something when catch error
        }


        var _self = this;
        wx.request({
            url: config.basepath+'channels/purchased.json',
            header: {
                "X-User-Email": user_email,
                "X-User-Token": user_token,
                'Content-Type': 'application/json'
            },
            method: 'GET',
            dataType:'json',
            success: function (res) {
                // success
                console.log("purchased.js  request purchased api success" + res.data);
                _self.setData({
                    "GankDatas": res.data,
                });
            },
            fail: function () {
                // fail
                console.log('purchased.js purchased fail');
            },
            complete: function () {
                // complete
                console.log('app.js purchased complete');
            }
        })

    },



    data: {
        toView: 'red',
        scrollTop: 100,
        userInfo: {},
        GankDatas: '',
        indicatorDots: true,
        autoplay: true,
        interval: 2000,
        duration: 1000,
    },




    upper: function (e) {
        console.log(e)
    },
    lower: function (e) {
        console.log(e)
    },
    scroll: function (e) {
        console.log(e)
    },
    imageError: function (e) {
        console.log('image3发生error事件，携带值为', e.detail.errMsg)
    },

    toast: function () {
        wx.navigateTo({
            url: '../details/details'
        })
    },
});

