const config = require("../../config");
var invite;//邀请码
var that;
var inviter;//邀请者



Page({

    onShareAppMessage: function () {
        return {
            title: '好友发红包啦!',
            desc: '邀请你加入yogalemon',
            path: '/pages/red/red?share=true&invite=' + invite + '&inviter=' + inviter
        }
    },

    onLoad: function (options) {

        try {
            invite = wx.getStorageSync(config.invite_code)
            var nickName = wx.getStorageSync(config.nickName)
            var uname = wx.getStorageSync(config.uname)
        } catch (e) {
            // Do something when catch error
        }
        that = this;
        that.setData({
            nickName: nickName,
            uname: uname,
            share: options.share,
            inviter: options.inviter,
        });

        if (invite == undefined) {
            invite = options.invite;
        }

        if (options.share == undefined) {
            inviter = wx.getStorageSync(config.uname);
        }

        console.log('invite = ' + invite);
        console.log('inviter = ' + inviter);
        console.log('options.inviter = ' + options.inviter);
        console.log('options.share = ' + options.share);
        console.log('this.data.inviter = ' + this.data.inviter);
        
    },
    data: {
        hidden: false,
        hiddenred: true,
        share: '',
        nickName: '',
        uname: '',
        phone: '',
        inviter: '',
    },
    cancel: function () {

    },
    confirm: function () {
        this.setData({
            hidden: true
        });
    },

    confirmred: function () {
        wx.switchTab({
            url: '../index/index'
        })

        this.setData({
            hiddenred: true
        });
    },
    phoneInput: function (e) {
        this.setData({
            phone: e.detail.value
        })
    },

    getRed: function () {
        console.log(this.data.phone.length)
        if (this.data.phone.length == 0) {
            wx.showToast({
                title: '请输入手机号',
                icon: 'success',
                duration: 500
            })
            return;
        }

        var phoneNumber = this.data.phone;
        var reg = /^1[0-9]{10}$/;
        var isPhone = reg.test(phoneNumber);
        if (!isPhone) {
            wx.showToast({
                title: '请输入正确的手机号',
                icon: 'success',
                duration: 500
            })
            return;
        }


        wx.request({
            url: config.basepath + 'users/accept_invite.json',
            data: {
                phone: phoneNumber,
                code: invite,
                user: { phone: phoneNumber }
            },
            method: 'POST',
            dataType: 'json',
            header: {
                'Content-Type': 'application/json'
            },
            success: function (res) {
                // success
                console.log(' red.js invite success' + res.data)
                var json = res.data;
                console.log('json.status = ' + json.status);
                console.log('json.error = ' + json.error);

                if (json.status == "FAIL") {
                    if (json.error == "User existed") {
                        wx.showToast({
                            title: '用户已存在',
                            icon: 'success',
                            duration: 500
                        })

                    } else if (json.error == "Invalid code") {
                        wx.showToast({
                            title: '邀请码错误',
                            icon: 'success',
                            duration: 500
                        })
                    }
                } else if (json.status == "OK") {
                    wx.showToast({
                        title: '领取成功',
                        icon: 'success',
                        duration: 500
                    });
                    that.setData({
                        hiddenred: false
                    });
                }
            },
            fail: function () {
                // fail
                console.log('red.js invite fail')
            },
            complete: function () {
                // complete
                console.log('red.js invite complete')
            }
        })
    }

});

